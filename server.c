#define GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <pthread.h>
#include <fcntl.h>
#include <ncurses.h>

#include "list.h"
#include "server.h"
#include "communication.h"
#include "task_list.h"
#include "user.h"
#include "utils.h"

#define QUIT_CHAR 'q'

const char DEFAULT_HOST[] = "127.0.0.1";
const int DEFAULT_PORT = 1370;

char is_server_working = 1;

ClientCache client_caches[MAX_USERS] = {0};

int update_message_socket;
pthread_mutex_t update_message_mutex;

void send_update_message(int updated_list_id, struct sockaddr_in *client)
{
  // printw("send msg %d to %d:%hu", updated_list_id, client->sin_addr.s_addr, client->sin_port);
  sendto(update_message_socket, &updated_list_id, sizeof(updated_list_id), 0, (struct sockaddr *)client, sizeof(*client));
}

void process_sign_in(Client *client, Command *com, Response *resp)
{
  client->user_id = user_get_or_create(com->data.lid.username);
  client->cc = &client_caches[client->user_id];

  pthread_mutex_lock(&client->cc->mutex);
  if (client->cc->task_lists_fd == NULL)
    client->cc->task_lists_fd = open_or_create_task_list_db(client->user_id);
  pthread_mutex_unlock(&client->cc->mutex);

  pthread_mutex_lock(&update_message_mutex);
  int new_session_id;
  for (new_session_id = 0; new_session_id < MAX_SESSIONS_PER_USER; new_session_id++)
  {
    if (client->cc->addresses[new_session_id].sin_port == 0)
      break;
  }
  if (new_session_id < MAX_SESSIONS_PER_USER)
  {
    client->cc->addresses[new_session_id].sin_family = AF_INET;
    client->cc->addresses[new_session_id].sin_addr.s_addr = client->addr;
    client->cc->addresses[new_session_id].sin_port = htons(com->data.lid.listener_port);
  }
  pthread_mutex_unlock(&update_message_mutex);
  if (new_session_id >= MAX_SESSIONS_PER_USER)
  {
    resp->code = NOT_PERMITTED;
    strncpy(resp->data.ed.description, "User sessions limit exceeded", ERROR_DESCRIPTION_LEN);
    return;
  }

  client->port = com->data.lid.listener_port;
  client->auth = true;

  // printw("Log in! Username=%s id=%d port=%hu\n", com->data.lid.username, client->user_id, com->data.lid.listener_port);

  resp->code = SUCCESS;
}

void process_get_lists(Client *client, Response *resp)
{
  // printf("User (%d): get lists\n", client->user_id);

  TaskList *task_lists[MAX_TASK_LISTS_PER_USER] = {NULL};

  pthread_mutex_lock(&client->cc->mutex);
  int task_lists_len = get_task_lists(client->cc->task_lists_fd, task_lists);
  pthread_mutex_unlock(&client->cc->mutex);

  int task_list_count = 0;
  for (int task_list_id = 0; task_list_id < MAX_TASK_LISTS_PER_USER && task_list_count < task_lists_len; task_list_id++)
  {
    if (task_lists[task_list_id] != NULL)
    {
      memcpy(&resp->data.gld.task_lists[task_list_count], task_lists[task_list_id], sizeof(TaskList));

      free(task_lists[task_list_id]);
      task_lists[task_list_id] = NULL;

      task_list_count++;
    }
  }

  resp->data.gld.len = task_lists_len;

  resp->code = SUCCESS;
}

void process_create_list(Client *client, Command *com, Response *resp)
{
  // printw("User (%d): create list\n", client->user_id);
  FILE *task_list_fds[MAX_TASK_LISTS_PER_USER];

  pthread_mutex_lock(&client->cc->mutex);
  int task_list_id = create_task_list(client->user_id, client->cc->task_lists_fd, task_list_fds, com->data.cld.name);
  pthread_mutex_unlock(&client->cc->mutex);

  if (task_list_id < 0)
  {
    resp->code = EXCEEDED_LIMIT;
    strncpy(resp->data.ed.description, "Task lists limit exceeded", ERROR_DESCRIPTION_LEN);
  }
  else
  {
    resp->data.cld.id = task_list_id;
    resp->code = SUCCESS;

    pthread_mutex_lock(&update_message_mutex);
    for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
    {
      struct sockaddr_in *addr = &client->cc->addresses[sess_id];
      if (addr->sin_port != 0 && (addr->sin_addr.s_addr != client->addr || addr->sin_port != client->port))
        send_update_message(-1, &client->cc->addresses[sess_id]);
    }
    pthread_mutex_unlock(&update_message_mutex);
  }
}

void process_delete_list(Client *client, Command *com, Response *resp)
{
  // printf("User (%d): delete list\n", client->user_id);
  pthread_mutex_lock(&client->cc->mutex);
  remove_task_list(client->user_id, client->cc->task_lists_fd, client->cc->task_list_fds, com->data.dld.list_id);
  pthread_mutex_unlock(&client->cc->mutex);

  resp->code = SUCCESS;

  pthread_mutex_lock(&update_message_mutex);
  for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
  {
    struct sockaddr_in *addr = &client->cc->addresses[sess_id];
    if (addr->sin_port != 0 && (addr->sin_addr.s_addr != client->addr || addr->sin_port != client->port))
      send_update_message(-1, &client->cc->addresses[sess_id]);
  }
  pthread_mutex_unlock(&update_message_mutex);
}

void process_edit_list(Client *client, Command *com, Response *resp)
{
  // printf("User (%d): edit list\n", client->user_id);
  pthread_mutex_lock(&client->cc->mutex);
  update_task_list(client->cc->task_lists_fd, com->data.eld.list_id, com->data.eld.name);
  pthread_mutex_unlock(&client->cc->mutex);

  resp->code = SUCCESS;

  pthread_mutex_lock(&update_message_mutex);
  for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
  {
    struct sockaddr_in *addr = &client->cc->addresses[sess_id];
    if (addr->sin_port != 0 && (addr->sin_addr.s_addr != client->addr || addr->sin_port != client->port))
      send_update_message(-1, &client->cc->addresses[sess_id]);
  }
  pthread_mutex_unlock(&update_message_mutex);
}

void process_get_tasks(Client *client, Command *com, Response *resp)
{
  // printf("User (%d): get task list\n", client->user_id);
  Task *tasks[MAX_TASKS_PER_LIST] = {NULL};

  pthread_mutex_lock(&client->cc->mutex);
  int task_list_len = get_task_list(client->user_id, client->cc->task_list_fds, com->data.gtd.list_id, tasks);
  pthread_mutex_unlock(&client->cc->mutex);

  if (task_list_len == -1)
  {
    resp->code = NOT_FOUND;
    strcpy(resp->data.ed.description, "Task list not found");
  }
  else
  {
    resp->data.gtd.len = task_list_len;

    int task_count = 0;
    for (int task_id = 0; task_id < MAX_TASKS_PER_LIST && task_count < task_list_len; task_id++)
    {
      if (tasks[task_id] != NULL)
      {
        memcpy(&resp->data.gtd.tasks[task_count], tasks[task_id], sizeof(Task));

        free(tasks[task_id]);
        tasks[task_id] = NULL;

        task_count++;
      }
    }

    resp->code = SUCCESS;
  }
}

void process_create_task(Client *client, Command *com, Response *resp)
{
  // printf("User (%d): create task\n", client->user_id);

  pthread_mutex_lock(&client->cc->mutex);
  int task_id = create_task(client->cc->task_list_fds, client->user_id, com->data.ctd.list_id, com->data.ctd.title, com->data.ctd.description, com->data.ctd.deadline);
  pthread_mutex_unlock(&client->cc->mutex);

  if (task_id < 0)
  {
    resp->code = EXCEEDED_LIMIT;
    strncpy(resp->data.ed.description, "Tasks per list limit exceeded", ERROR_DESCRIPTION_LEN);
  }
  else
  {
    resp->data.ctd.id = task_id;
    resp->code = SUCCESS;

    pthread_mutex_lock(&update_message_mutex);
    for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
    {
      struct sockaddr_in *addr = &client->cc->addresses[sess_id];
      if (addr->sin_port != 0 && (addr->sin_addr.s_addr != client->addr || addr->sin_port != client->port))
        send_update_message(com->data.ctd.list_id, &client->cc->addresses[sess_id]);
    }
    pthread_mutex_unlock(&update_message_mutex);
  }
}

void process_delete_task(Client *client, Command *com, Response *resp)
{
  // printf("User (%d): delete task\n", client->user_id);

  pthread_mutex_lock(&client->cc->mutex);
  int remove_status = remove_task(client->cc->task_list_fds, client->user_id, com->data.dtd.list_id, com->data.dtd.task_id);
  pthread_mutex_unlock(&client->cc->mutex);

  if (remove_status)
  {
    resp->code = NOT_FOUND;
    strncpy(resp->data.ed.description, "Task list not found", ERROR_DESCRIPTION_LEN);
  }
  else
  {
    resp->code = SUCCESS;

    pthread_mutex_lock(&update_message_mutex);
    for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
    {
      struct sockaddr_in *addr = &client->cc->addresses[sess_id];
      if (addr->sin_port != 0 && (addr->sin_addr.s_addr != client->addr || addr->sin_port != client->port))
        send_update_message(com->data.dtd.list_id, &client->cc->addresses[sess_id]);
    }
    pthread_mutex_unlock(&update_message_mutex);
  }
}

void process_edit_task(Client *client, Command *com, Response *resp)
{
  // printf("User (%d): edit task\n", client->user_id);

  time_t *deadline = com->data.etd.deadline == 0 ? NULL : &com->data.etd.deadline;

  pthread_mutex_lock(&client->cc->mutex);
  int update_status = update_task(client->cc->task_list_fds, client->user_id, com->data.etd.list_id, com->data.etd.task_id, com->data.etd.title, com->data.etd.description, deadline);
  pthread_mutex_unlock(&client->cc->mutex);

  if (update_status)
  {
    resp->code = NOT_FOUND;
    strncpy(resp->data.ed.description, "Task list not found", ERROR_DESCRIPTION_LEN);
  }
  else
  {
    resp->code = SUCCESS;

    pthread_mutex_lock(&update_message_mutex);
    for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
    {
      struct sockaddr_in *addr = &client->cc->addresses[sess_id];
      if (addr->sin_port != 0 && (addr->sin_addr.s_addr != client->addr || addr->sin_port != client->port))
        send_update_message(com->data.etd.list_id, &client->cc->addresses[sess_id]);
    }
    pthread_mutex_unlock(&update_message_mutex);
  }
}

Response *process_command(Client *client, Command *com)
{
  Response *resp = (Response *)malloc(sizeof(Response));

  if (client->auth == false && com->code != COMMAND_SIGN_IN)
  {
    resp->code = NOT_PERMITTED;
    strcpy(resp->data.ed.description, "Unauthorized");
    return resp;
  }

  switch (com->code)
  {
  case COMMAND_SIGN_IN:
    process_sign_in(client, com, resp);
    break;

  case COMMAND_GET_LISTS:
    process_get_lists(client, resp);
    break;

  case COMMAND_CREATE_LIST:
    process_create_list(client, com, resp);
    break;
  case COMMAND_DELETE_LIST:
    process_delete_list(client, com, resp);
    break;
  case COMMAND_EDIT_LIST:
    process_edit_list(client, com, resp);
    break;

  case COMMAND_GET_TASKS:
  {
    process_get_tasks(client, com, resp);
    break;
  }
  case COMMAND_CREATE_TASK:
    process_create_task(client, com, resp);
    break;
  case COMMAND_DELETE_TASK:
    process_delete_task(client, com, resp);
    break;
  case COMMAND_EDIT_TASK:
    process_edit_task(client, com, resp);
    break;
  default:
    perror("Unknown command");
    resp->code = NOT_FOUND;
    strcpy(resp->data.ed.description, "Unknown command");
  }

  return resp;
}

void manage_client(void *data)
{
  Client *client = (Client *)data;

  Command com;
  int n = 0;
  while (is_server_working)
  {
    n = recv(client->socket, &com, sizeof(Command), O_NONBLOCK);
    if (n > 0)
    {
      Response *resp = process_command(client, &com);

      ssize_t total_bytes_written = 0;
      while (total_bytes_written != sizeof(Response))
      {
        ssize_t bytes_written = write(client->socket,
                                      &((char *)(resp))[total_bytes_written],
                                      min(sizeof(Response) - total_bytes_written, 512));
        if (bytes_written == -1)
        {
          perror("write ");
          break;
        }
        total_bytes_written += bytes_written;
      }

      free(resp);
    }
  }

  // remove client from updating addresses list
  for (int sess_id = 0; sess_id < MAX_SESSIONS_PER_USER; sess_id++)
    if (client->cc->addresses[sess_id].sin_addr.s_addr == client->addr)
      client->cc->addresses[sess_id].sin_port = 0;

  // close user socket
  close(client->socket);

  free(client);
}

int create_listener(const char *host, int port)
{
  struct sockaddr_in srv_addr;

  memset(&srv_addr, '0', sizeof(srv_addr));
  srv_addr.sin_family = AF_INET;
  srv_addr.sin_addr.s_addr = inet_addr(host);
  srv_addr.sin_port = htons(port);

  int listener = socket(AF_INET, SOCK_STREAM, 0);
  bind(listener, (struct sockaddr *)&srv_addr, sizeof(srv_addr));

  fcntl(listener, F_SETFL, fcntl(listener, F_GETFL, NULL) | O_NONBLOCK);
  listen(listener, SOMAXCONN);

  return listener;
}

void run_server(const char *host, unsigned short port)
// Host and port should be NULL for default value
{
  // set default values
  if (host == NULL)
    host = DEFAULT_HOST;

  if (port == 0)
    port = DEFAULT_PORT;

  // read users info
  user_read_users_list();

  // threads control
  List threads = EMPTY_LIST;

  // create listener
  int listener = create_listener(host, port);

  int conn_socket = 0;
  Client *client = NULL;
  pthread_t *thread = NULL;

  // init ncurses
  initscr();
  cbreak();
  nodelay(stdscr, TRUE);
  printw("Server started! Press 'q' to stop it.\n");
  refresh();

  // init mutexes for uses caches
  for (int i = 0; i < MAX_USERS; i++)
    pthread_mutex_init(&client_caches[i].mutex, NULL);

  // init update message socket
  if ((update_message_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
  {
    perror("socket()");
    exit(1);
  }
  // init update message mutex
  pthread_mutex_init(&update_message_mutex, NULL);

  struct sockaddr_in client_addr;
  int client_addr_len = sizeof(client_addr);
  while (getch() != QUIT_CHAR)
  {
    conn_socket = accept(listener, (struct sockaddr *)&client_addr, &client_addr_len);
    if (conn_socket > 0)
    {
      printw("new connection established\n");
      fcntl(conn_socket, F_SETFL, fcntl(conn_socket, F_GETFL, NULL) | O_NONBLOCK);

      client = (Client *)malloc(sizeof(Client));
      memset(client, 0, sizeof(Client));
      thread = (pthread_t *)malloc(sizeof(pthread_t));

      client->addr = client_addr.sin_addr.s_addr;
      client->socket = conn_socket;
      pthread_create(thread, NULL, (void *(*)(void *)) & manage_client, (void *)client);

      list_append(&threads, thread);
    }
  }
  // stop all threads
  is_server_working = 0;

  // close listener socket
  close(listener);

  // end ncurses window mode
  endwin();

  // and join all of them
  for (int i = 0; i < threads.length; i++)
  {
    pthread_join(*(pthread_t *)list_get(&threads, i), NULL);
  }

  // destroy update message mutex
  pthread_mutex_destroy(&update_message_mutex);

  // close update message socket
  close(update_message_socket);

  //clean cache
  for (int i = 0; i < MAX_USERS; i++)
  {
    ClientCache *cc = &client_caches[i];
    if (cc->task_lists_fd != NULL)
      fclose(cc->task_lists_fd);

    for (int j = 0; j < MAX_TASK_LISTS_PER_USER; j++)
      if (cc->task_list_fds[j] != NULL)
        fclose(cc->task_list_fds[j]);

    pthread_mutex_destroy(&cc->mutex);
  }

  user_release_all_resources();

  clean_list(&threads);
}

// int main()
// {
//   start_server("0.0.0.0", 1370);
//   return 0;
// }
