START_PATH=$PWD
MY_PATH="`dirname \"$0\"`"
cd $MY_PATH

gcc -g ../assert.c ../file_access.c ../user.c -o user -I ../include
# gdb ./user
./user

rm ./user
rm ./test_data/users_test.dat

cd $START_PATH
