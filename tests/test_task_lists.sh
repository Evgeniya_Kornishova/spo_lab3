START_PATH=$PWD
MY_PATH="`dirname \"$0\"`"
cd $MY_PATH

gcc -g ../assert.c ../file_access.c ../task_list.c -o task_list -I ../include
# gdb ./task_list
./task_list

rm ./task_list
rm ./db/*

cd $START_PATH
