#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include "client.h"
#include "communication.h"
#include "client_graphic.h"
#include "client_commands.h"
#include "task_list.h"
#include "user.h"
#include "utils.h"

// main data socket
int sock;
int update_listener_socket;

// mutex
pthread_mutex_t mutex;
// thread
pthread_t update_listener_thread;

// user data
char username[MAX_USERNAME_LEN];
TaskList task_lists[MAX_TASK_LISTS_PER_USER];
Task task_list[MAX_TASKS_PER_LIST];
int task_lists_len = 0;
int task_list_len = 0;

// windows
WINDOW *main_window;
WINDOW *input_window;
WINDOW *details_window;
WINDOW *alert_window;
WINDOW *menu_window;

// interactive variables
int list_position = 0;
int task_position = 0;
int current_menu = 0;
char is_working = 1;

// functions
void add_element();
void get_element();
void delete_element();

void move_up();
void move_down();
void move_left();
void move_right();

void print_menu();
void print_task_details();
void print_app();

int fill_task_list_form(char **name);
int fill_task_form(char **title, char **deadline, char **description);

void abort_client()
{
  is_working = 0;
  // stop listener thread
  pthread_join(update_listener_thread, NULL);
  // release listener mutex
  pthread_mutex_destroy(&mutex);
  // close listener socket
  close(update_listener_socket);

  delwin(menu_window);
  delwin(input_window);
  delwin(details_window);
  delwin(main_window);
  endwin();

  close(sock);
}

void print_menu()
{
  werase(menu_window);

  // Menu elements offsets
  int lists_x_offset = 0;
  int tasks_x_offset = 27;
  int y_offsets[6] = {0, 3, 6, 9, 12, 15};

  // if menu is empty
  if (task_lists_len == 0)
  {
    drawMenuBox(menu_window, y_offsets[0], lists_x_offset, MENU_EL_FOCUS, "     . . .      ");
    wrefresh(menu_window);
    return;
  }

  bool first_el_hidden = task_lists_len > 6 && list_position > 2;
  bool last_el_hidden = task_lists_len > 6 && list_position < task_lists_len - 3;

  int start_id = max(list_position - 5, 0);
  int end_id = min(list_position + 5, task_lists_len - 1);

  while (end_id - start_id + 1 > 6)
  {
    if (end_id - list_position > list_position - start_id)
      end_id--;
    else
      start_id++;
  }

  // draw lists menu
  int menu_position = 0;
  // first hidden element
  if (first_el_hidden)
  {
    drawMenuBox(menu_window, y_offsets[0], lists_x_offset, MENU_EL_NORMAL, "     . . .      ");
    start_id++;
    menu_position++;
  }

  // last hidden element
  if (last_el_hidden)
  {
    drawMenuBox(menu_window, y_offsets[5], lists_x_offset, MENU_EL_NORMAL, "     . . .      ");
    end_id--;
  }

  int state;
  int active_state = (current_menu == 0) ? MENU_EL_FOCUS : MENU_EL_SELECTED;
  for (int id = start_id; id <= end_id; id++)
  {
    state = (id == list_position) ? active_state : MENU_EL_NORMAL;

    drawMenuBox(menu_window, y_offsets[menu_position++], lists_x_offset, state, task_lists[id].name);
  }

  // calculate task menu
  // if menu empty
  active_state = (current_menu == 1) ? MENU_EL_FOCUS : MENU_EL_SELECTED;
  if (task_list_len == 0)
  {
    drawMenuBox(menu_window, y_offsets[0], tasks_x_offset, active_state, "     . . .      ");
    wrefresh(menu_window);
    return;
  }

  first_el_hidden = (task_list_len > 6) && (task_position > 2);
  last_el_hidden = (task_list_len > 6) && (task_position < task_list_len - 3);

  start_id = max(task_position - 5, 0);
  end_id = min(task_position + 5, task_list_len - 1);

  while (end_id - start_id + 1 > 6)
  {
    if (end_id - task_position > task_position - start_id)
      end_id--;
    else
      start_id++;
  }

  menu_position = 0;
  // draw tasks menu
  if (first_el_hidden)
  {
    drawMenuBox(menu_window, y_offsets[0], tasks_x_offset, MENU_EL_NORMAL, "     . . .      ");
    start_id++;
    menu_position++;
  }

  if (last_el_hidden)
  {
    drawMenuBox(menu_window, y_offsets[5], tasks_x_offset, MENU_EL_NORMAL, "     . . .      ");
    end_id--;
  }

  for (int id = start_id; id <= end_id; id++)
  {
    state = (id == task_position) ? active_state : MENU_EL_NORMAL;
    drawMenuBox(menu_window, y_offsets[menu_position++], tasks_x_offset, state, task_list[id].title);
  }

  wrefresh(menu_window);
}

// connect to server
void connect_server(const char *srv_host, unsigned short srv_port)
{
  // get hostname
  struct hostent *hostname = gethostbyname(srv_host);
  if (hostname == NULL)
  {
    draw_alert(alert_window, "INPUT ERROR", "Host name is incorrect");
    abort_client();
    exit(0);
  }

  // fill sockaddr structure
  struct sockaddr_in server;

  server.sin_family = AF_INET;
  server.sin_port = htons(srv_port);
  server.sin_addr.s_addr = *((unsigned long *)hostname->h_addr);

  // creat socket
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
  {
    draw_alert(alert_window, "CONNECTION ERROR", "Socket creation error");
    abort_client();
    exit(0);
  }

  // connect to server
  if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
  {
    draw_alert(alert_window, "CONNECTION ERROR", "Server do not reponse");
    abort_client();
    exit(0);
  }
}

void print_task_details()
{
  wclear(details_window);
  box(details_window, '*', '*');

  const int x_offset = 2;

  const int title_offset = 1;
  const int created_at_offset = 2;
  const int desc_offset = 3;
  const int deadline_offset = 16;

  if (task_list_len > 0)
  {
    Task *task = &task_list[task_position];

    // format title
    mvwaddstr(details_window, title_offset, x_offset, task->title);

    // format created at
    char formatted_created_at[28] = {0};
    struct tm *timeinfo;
    timeinfo = localtime(&task->created_at);
    strftime(formatted_created_at, 28, "Created at %d.%m:%Y %H:%M", timeinfo);
    mvwaddstr(details_window, created_at_offset, x_offset, formatted_created_at);

    // format details
    char formatted_details[42] = {0};
    for (int i = 0; i < 13; i++)
    {
      strncpy(formatted_details, &task->description[i * 41], 41);
      mvwaddstr(details_window, desc_offset + i, x_offset, formatted_details);
    }

    // format deadline
    char formatted_deadline[27] = {0};
    timeinfo = localtime(&task->deadline);
    strftime(formatted_deadline, 27, "Deadline: %d.%m:%Y %H:%M", timeinfo);
    mvwaddstr(details_window, deadline_offset, x_offset, formatted_deadline);
  }
  wrefresh(details_window);
}

int fill_task_list_form(char **name)
{
  const char name_input_label[] = "Name: ";

  int start_position_x = 8;
  int start_position_y = 1;

  mvwaddstr(input_window, start_position_y, 2, name_input_label);
  curs_set(1);
  wmove(input_window, start_position_y, start_position_x);
  wrefresh(input_window);

  *name = (char *)malloc(MAX_TASK_LIST_NAME_LEN);
  memset(*name, 0, MAX_TASK_LIST_NAME_LEN);

  int limit = MAX_TASK_LIST_NAME_LEN;

  bool input_finished = false;
  int position = 0;
  int ch;
  while (!input_finished)
  {
    switch (ch = getch())
    {
    case 10: // Enter
      input_finished = true;
      (*name)[position + 1] = 0;
      break;
    case KEY_BACKSPACE:
      if (position > 0)
      {
        position--;
        int x = start_position_x + position;
        int y = start_position_y;
        mvwaddch(input_window, y, x, ' ');
        wmove(input_window, y, x);
      }
      break;
    case 27: // Esc
      free(*name);
      curs_set(0);
      werase(input_window);
      box(input_window, '#', '#');
      wrefresh(input_window);
      return -1;
      break;
    default:
      if (ch >= 0x20 && ch < 0x7f)
        if (position < limit - 1)
        {
          int x = start_position_x + position;
          int y = start_position_y;

          mvwaddch(input_window, y, x, ch);
          (*name)[position] = ch;

          position++;

          if (position < limit - 1)
          {
            x = start_position_x + position;
            y = start_position_y;
            wmove(input_window, y, x);
          }
        }
    }

    wrefresh(input_window);
  }
  curs_set(0);
  werase(input_window);
  box(input_window, '#', '#');
  wrefresh(input_window);
}

int fill_task_form(char **title, char **deadline, char **description)
{
  const char title_input_label[] = "Title: ";
  const char deadline_input_label[] = "Deadline (dd.mm.yyyy hh.mm): ";
  const char description_input_label[] = "Description: ";

  int base_start_position_x = 2;

  int start_position_y[3];
  start_position_y[0] = 1;
  start_position_y[1] = 2;
  start_position_y[2] = 3;

  mvwaddstr(input_window, start_position_y[0], base_start_position_x, title_input_label);
  mvwaddstr(input_window, start_position_y[1], base_start_position_x, deadline_input_label);
  mvwaddstr(input_window, start_position_y[2], base_start_position_x, description_input_label);

  int start_position_x[3];
  start_position_x[0] = 9;
  start_position_x[1] = 31;
  start_position_x[2] = 15;

  curs_set(1);
  wrefresh(input_window);

  int finish_position_x = 97;

  int limit[3];
  limit[0] = MAX_TASK_TITLE_LEN;
  limit[1] = 17;
  limit[2] = MAX_TASK_DESC_LEN;

  char *buffer[3];
  bool form_confirmed = false;
  for (int i = 0; i < 3; i++)
  {
    buffer[i] = (char *)malloc(limit[i]);
    memset(buffer[i], 0, limit[i]);

    if (form_confirmed)
      continue;

    bool input_finished = false;
    int position = 0;
    int ch;
    wmove(input_window, start_position_y[i], start_position_x[i]);
    wrefresh(input_window);

    while (!input_finished)
    {
      switch (ch = getch())
      {
      case 0x9: // Tab
        if (i < 2)
        {
          input_finished = true;
          buffer[i][position + 1] = 0;
        }
        break;

      case 10: // Enter
        input_finished = true;
        buffer[i][position + 1] = 0;
        form_confirmed = true;

        break;
      case KEY_BACKSPACE:
        if (position > 0)
        {
          position--;
          int x = base_start_position_x + (position + start_position_x[i] - base_start_position_x) % (finish_position_x - base_start_position_x);
          int y = start_position_y[i] + (position + start_position_x[i] - base_start_position_x) / (finish_position_x - base_start_position_x);
          mvwaddch(input_window, y, x, ' ');
          wmove(input_window, y, x);
        }
        break;
      case 27: // Esc
        free(buffer[0]);
        free(buffer[1]);
        free(buffer[2]);
        werase(input_window);
        box(input_window, '#', '#');
        wrefresh(input_window);
        curs_set(0);
        return -1;
        break;
      default:
        if ((ch >= 0x20 && ch < 0x7f && (i != 1)) || (i == 1 && (ch >= '0' && ch <= '9' || ch == '.' || ch == ':' || ch == ' ')))
        {
          if (position < limit[i] - 1)
          {
            int x = base_start_position_x + (position + start_position_x[i] - base_start_position_x) % (finish_position_x - base_start_position_x);
            int y = start_position_y[i] + (position + start_position_x[i] - base_start_position_x) / (finish_position_x - base_start_position_x);
            mvwaddch(input_window, y, x, ch);
            buffer[i][position] = ch;
            position++;

            if (position < limit[i] - 1)
            {
              x = base_start_position_x + (position + start_position_x[i] - base_start_position_x) % (finish_position_x - base_start_position_x);
              y = start_position_y[i] + (position + start_position_x[i] - base_start_position_x) / (finish_position_x - base_start_position_x);

              wmove(input_window, y, x);
            }
          }
        }
        break;
      }
      wrefresh(input_window);
    }
  }
  *title = buffer[0];
  *deadline = buffer[1];
  *description = buffer[2];
  curs_set(0);
  werase(input_window);
  box(input_window, '#', '#');
  wrefresh(input_window);
  return 0;
}

void add_element()
{
  if (current_menu == 0)
  {
    char *name;
    int status = fill_task_list_form(&name);
    if (status < 0)
      return;

    if (strncmp(trim(name), "", MAX_TASK_LIST_NAME_LEN) == 0)
    {
      free(name);
      name = NULL;
      draw_alert(alert_window, "INPUT ERROR", "Name is required field!");
      return;
    }

    com_create_list(name);
    com_get_lists();
    if (list_position >= task_lists_len)
    {
      list_position = max(task_lists_len - 1, 0);
      if (task_lists_len > 0)
        com_get_tasks(task_lists[list_position].id);
    }
    print_menu();
    print_task_details();
  }
  else
  {
    char *title;
    char *s_deadline;
    char *description;
    int status = fill_task_form(&title, &s_deadline, &description);

    if (status < 0)
      return;

    if (strncmp(trim(title), "", MAX_TASK_TITLE_LEN) == 0)
    {
      free(title);
      title = NULL;
      draw_alert(alert_window, "INPUT ERROR", "Title field should be specified");
      return;
    }

    time_t deadline = string_to_time_t(s_deadline);
    if (strncmp(trim(s_deadline), "", 17) != 0 && deadline == 0)
    {
      free(s_deadline);
      s_deadline = NULL;
      draw_alert(alert_window, "INPUT ERROR", "Deadline format is 'dd.mm.yyyy hh:hh'");
      return;
    }

    com_create_task(task_lists[list_position].id, title, description, deadline);
    com_get_tasks(task_lists[list_position].id);

    print_menu();
    print_task_details();
  }
}

void edit_element()
{
  if (current_menu == 0 && task_lists_len > 0)
  {
    char *name;
    int status = fill_task_list_form(&name);

    if (status < 0)
      return;

    if (strncmp(trim(name), "", MAX_TASK_LIST_NAME_LEN) == 0)
    {
      free(name);
      name = NULL;
      draw_alert(alert_window, "INPUT ERROR", "Name field can not be empty");
      return;
    }

    com_edit_list(task_lists[list_position].id, name);
    com_get_lists();

    if (list_position >= task_lists_len)
    {
      list_position = max(task_lists_len - 1, 0);
      if (task_lists_len > 0)
        com_get_tasks(task_lists[list_position].id);
    }

    print_menu();
    print_task_details();
  }
  else if (current_menu == 1 && task_list_len > 0)
  {
    char *title;
    char *s_deadline;
    char *description;
    int status = fill_task_form(&title, &s_deadline, &description);

    if (status < 0)
      return;

    if (strncmp(trim(title), "", MAX_TASK_TITLE_LEN) == 0)
    {
      free(title);
      title = NULL;
    }

    time_t deadline = string_to_time_t(s_deadline);
    if (strncmp(trim(s_deadline), "", 16) != 0 && deadline == 0)
    {
      free(s_deadline);
      s_deadline = NULL;
      draw_alert(alert_window, "INPUT_ERROR", "Deadline format is 'dd.mm.yyyy hh:hh'");
      return;
    }

    if (strncmp(trim(description), "", MAX_TASK_DESC_LEN) == 0)
    {
      free(description);
      description = NULL;
    }

    com_edit_task(task_lists[list_position].id, task_list[task_position].id, title, description, deadline);
    com_get_tasks(task_lists[list_position].id);

    print_menu();
    print_task_details();
  }
}

void delete_element()
{
  if (current_menu == 0 && task_lists_len > 0)
  {
    com_delete_list(task_lists[list_position].id);
    com_get_lists();

    if (list_position >= task_lists_len)
      list_position = max(task_lists_len - 1, 0);

    if (task_lists_len > 0)
      com_get_tasks(task_lists[list_position].id);

    print_menu();
    print_task_details();
  }
  else if (current_menu == 1 && task_list_len > 0)
  {
    com_delete_task(task_lists[list_position].id, task_list[task_position].id);
    com_get_tasks(task_lists[list_position].id);

    if (task_position >= task_list_len)
      task_position = max(task_list_len - 1, 0);

    print_menu();
    print_task_details();
  }
}

void move_up()
{
  if (current_menu == 0 && list_position > 0)
  {
    list_position--;
    com_get_tasks(task_lists[list_position].id);

    print_menu();
    print_task_details();
  }
  else if (current_menu == 1 && task_position > 0)
  {
    task_position--;

    print_menu();
    print_task_details();
  }
}
void move_down()
{
  if (current_menu == 0 && list_position < task_lists_len - 1)
  {
    list_position++;
    com_get_tasks(task_lists[list_position].id);

    print_menu();
    print_task_details();
  }
  else if (current_menu == 1 && task_position < task_list_len - 1)
  {
    task_position++;

    print_menu();
    print_task_details();
  }
}

void move_left()
{
  if (current_menu == 1)
  {
    current_menu = 0;
    print_menu();
  }
}

void move_right()
{
  if (current_menu == 0 && task_lists_len > 0)
  {
    current_menu = 1;
    print_menu();
    print_task_details();
  }
}

void print_app()
{
  clear();
  refresh();

  // print windows borders
  box(main_window, '#', '#');
  box(input_window, '#', '#');
  box(details_window, '*', '*');

  // print about info
  mvwaddstr(main_window, 1, 2, ABOUT);

  // print help info
  mvwaddstr(main_window, 21, 2, HELP_0);
  mvwaddstr(main_window, 22, 2, HELP_1);
  mvwaddstr(main_window, 23, 2, HELP_2);

  // print username
  mvwaddstr(main_window, 1, 83, username);

  // print titles
  mvwaddstr(main_window, 2, 9, LISTS_TITLE);
  mvwaddstr(main_window, 2, 36, TASKS_TITLE);
  mvwaddstr(main_window, 2, 55, TASK_TITLE);

  wrefresh(main_window);
  wrefresh(input_window);
  wrefresh(details_window);

  print_menu();
  print_task_details();
}

// return port
int create_update_listener_socket()
{
  // creat socket
  update_listener_socket = socket(AF_INET, SOCK_DGRAM, 0);
  if (update_listener_socket < 0)
  {
    draw_alert(alert_window, "NETWORK ERROR", "Socket creation error");
    abort_client();
    exit(0);
  }

  // fill sockaddr structure
  struct sockaddr_in server;

  server.sin_family = AF_INET;
  server.sin_port = 0;
  server.sin_addr.s_addr = INADDR_ANY;

  if (bind(update_listener_socket, (struct sockaddr *)&server, sizeof(server)) < 0)
  {
    draw_alert(alert_window, "NETWORK ERROR", "Socket bind error");
    abort_client();
    exit(0);
  }

  int server_addr_len = sizeof(server);
  if (getsockname(update_listener_socket, (struct sockaddr *)&server, &server_addr_len) < 0)
  {
    draw_alert(alert_window, "NETWORK ERROR", "Socket get info error");
    abort_client();
    exit(0);
  }

  fcntl(update_listener_socket, F_SETFL, fcntl(update_listener_socket, F_GETFL, NULL) | O_NONBLOCK);

  return ntohs(server.sin_port);
}

void *update_listener(void *args)
{
  // contains id of updated task_list or -1 if list created, updated or deleted
  int updated_list_id;
  while (is_working)
  {
    if (recv(update_listener_socket, &updated_list_id, sizeof(updated_list_id), O_NONBLOCK) > 0)
    {
      pthread_mutex_lock(&mutex);

      if (updated_list_id < 0)
      {
        com_get_lists();
        print_menu();
        print_task_details();
        // draw_alert(alert_window, "lists updated", "");
      }
      else if (updated_list_id == task_lists[list_position].id)
      {
        com_get_tasks(updated_list_id);
        print_menu();
        print_task_details();
        // draw_alert(alert_window, "tasks updated", "");
      }

      pthread_mutex_unlock(&mutex);
    }
  }
}

void run_client(const char *srv_host, unsigned short srv_port)
{
  while (strncmp(trim(username), "", MAX_USERNAME_LEN) == 0)
  {
    printf("Username: ");
    scanf("%15s", username);
  }

  // init ncurses
  initscr();
  curs_set(0);
  refresh();

  noecho();
  keypad(stdscr, TRUE);

  // windows offsets
  int main_offsetx = (COLS - MAIN_WINDOW_WIDTH) / 2;
  int main_offsety = (LINES - MAIN_WINDOW_HEIGHT) / 2;

  int input_offsetx = main_offsetx;
  int input_offsety = main_offsety + 24;

  int details_offsetx = main_offsetx + 54;
  int details_offsety = main_offsety + 3;

  int alert_offsetx = (COLS - ALERT_WINDOW_WIDTH) / 2;
  int alert_offsety = (LINES - ALERT_WINDOW_HEIGHT) / 2;

  int menu_offsetx = main_offsetx + 2;
  int menu_offsety = main_offsety + 3;

  main_window = newwin(MAIN_WINDOW_HEIGHT, MAIN_WINDOW_WIDTH, main_offsety, main_offsetx);
  input_window = subwin(main_window, INPUT_WINDOW_HEIGHT, INPUT_WINDOW_WIDTH, input_offsety, input_offsetx);
  details_window = subwin(main_window, DETAILS_WINDOW_HEIGHT, DETAILS_WINDOW_WIDTH, details_offsety, details_offsetx);
  alert_window = subwin(main_window, ALERT_WINDOW_HEIGHT, ALERT_WINDOW_WIDTH, alert_offsety, alert_offsetx);
  menu_window = subwin(main_window, MENU_WINDOW_HEIGHT, MENU_WINDOW_WIDTH, menu_offsety, menu_offsetx);

  start_color();
  init_colors();

  // intialize client socket
  connect_server(srv_host, srv_port);

  // init update listener socket
  unsigned short update_listener_port = create_update_listener_socket();

  // login
  com_sign_in(username, update_listener_port);
  com_get_lists();
  if (task_lists_len > 0)
    com_get_tasks(task_lists[0].id);

  print_app();

  // init mutex
  pthread_mutex_init(&mutex, NULL);
  // start listener thread
  pthread_create(&update_listener_thread, NULL, update_listener, NULL);

  int c;
  while (is_working)
  {
    c = getch();
    pthread_mutex_lock(&mutex);
    switch (c)
    {
    case KEY_UP:
      move_up();
      break;
    case KEY_DOWN:
      move_down();
      break;
    case KEY_LEFT:
      move_left();
      break;
    case KEY_RIGHT:
      move_right();
      break;
    case 27: // ESC
      is_working = 0;
      break;
    case '+':
      add_element();
      break;
    case 'e':
      edit_element();
      break;
    case KEY_DC: // Del
      delete_element();
      break;
    }
    pthread_mutex_unlock(&mutex);
  }

  // stop listener thread
  pthread_join(update_listener_thread, NULL);
  // release listener mutex
  pthread_mutex_destroy(&mutex);
  // close listener socket
  close(update_listener_socket);

  delwin(menu_window);
  delwin(input_window);
  delwin(details_window);
  delwin(main_window);
  endwin();

  close(sock);
}
