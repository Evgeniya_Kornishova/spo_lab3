#pragma once
#define MAX_USERS 256
#define MAX_USERNAME_LEN 16

typedef struct
{
  char name[MAX_USERNAME_LEN];
  unsigned int id;
} User;

void user_read_users_list();
unsigned int user_create(char *username);
unsigned int user_get_or_create(char *username);
void user_release_all_resources();
