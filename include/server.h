#pragma once

#include <stdio.h>
#include <netinet/in.h>

#include "list.h"
#include "task_list.h"

#define MAX_SESSIONS_PER_USER 8

typedef struct ClientCache
{
  FILE *task_lists_fd;
  FILE *task_list_fds[MAX_TASK_LISTS_PER_USER];
  pthread_mutex_t mutex;
  struct sockaddr_in addresses[MAX_SESSIONS_PER_USER];
} ClientCache;

typedef struct
{
  unsigned int user_id;
  int socket;
  bool auth;
  in_addr_t addr;
  unsigned short port;
  ClientCache *cc;
} Client;

void run_server(const char *host, unsigned short port);
