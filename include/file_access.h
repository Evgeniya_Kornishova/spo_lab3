#pragma once
#include <stdio.h>

#define DB_TASK_LIST_TABLE_OFFSET 17
#define DB_TASK_TABLE_OFFSET 1064
#define DB_USER_TABLE_OFFSET 32

#define DB_EMPTY (char)0x00
#define DB_FULL (char)0xff

void db_init(FILE *fd, size_t offset, size_t max_records);
int db_create(FILE *fd, char *data, size_t offset, size_t max_records);
void db_delete(FILE *fd, int id, size_t offset);
size_t db_get(FILE *fd, char **data, int id, size_t offset);
void db_update(FILE *fd, char *data, int id, size_t offset);
