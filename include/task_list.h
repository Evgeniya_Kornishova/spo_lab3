#pragma once

#include <stdio.h>
#include <time.h>

#define MAX_TASK_LIST_NAME_LEN 16
#define MAX_TASK_TITLE_LEN 16
#define MAX_TASK_DESC_LEN 512
#define MAX_TASKS_PER_LIST 256
#define MAX_TASK_LISTS_PER_USER 256

typedef struct
{
  unsigned char id;
  char name[MAX_TASK_LIST_NAME_LEN];
} TaskList;

typedef struct
{
  unsigned char id;
  char title[MAX_TASK_TITLE_LEN];
  time_t created_at;
  time_t deadline;
  char description[MAX_TASK_DESC_LEN];
} Task;

FILE *open_or_create_task_list_db(unsigned int user_id);
int get_task_lists(FILE *task_lists_fd, TaskList *task_lists[]);
int get_task_list(unsigned int user_id, FILE *task_list_fds[], unsigned char task_list_id, Task *task_list[]);
int create_task_list(unsigned int user_id, FILE *task_lists_fd, FILE *task_list_fds[], char *task_list_name);
int create_task(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id, char *title, char *description, time_t deadline);
void remove_task_list(unsigned int user_id, FILE *task_lists_fd, FILE *task_list_fds[], unsigned char task_list_id);
int remove_task(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id, unsigned char task_id);
void update_task_list(FILE *task_lists_fd, unsigned char task_list_id, char *task_list_name);
int update_task(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id, unsigned char task_id, char *title, char *description, time_t *deadline);
