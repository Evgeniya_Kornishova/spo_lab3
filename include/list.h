#pragma once

#define EMPTY_LIST \
  {                \
    NULL, NULL, 0  \
  }

typedef struct Node Node;

typedef struct
{
  Node *head;
  Node *tail;
  unsigned int length;
} List;

struct Node
{
  Node *next;
  void *value;
};

int list_append(List *list, void *value);
int list_remove(List *list, unsigned int pos);
int list_insert(List *list, unsigned int pos, void *value);
void *list_get(List *list, unsigned int pos);
void clean_list(List *list);
