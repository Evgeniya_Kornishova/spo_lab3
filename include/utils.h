#pragma once
#include <time.h>

int max(int x, int y);
int min(int x, int y);
char *ltrim(char *s);
char *rtrim(char *s);
char *trim(char *s);
time_t string_to_time_t(char *str_time);
int is_empty_string(char *s);
