#pragma once

#include <time.h>
#include <task_list.h>
#include <user.h>

#define ERROR_DESCRIPTION_LEN 32

// Command Data-----------------------------------------------

typedef struct
{
  char username[MAX_USERNAME_LEN];
  unsigned short listener_port;
} LogInData;

typedef struct
{
  char name[MAX_TASK_LIST_NAME_LEN];
} CreateListData;

typedef struct
{
  unsigned char list_id;
} DeleteListData;

typedef struct
{
  unsigned char list_id;
  char name[MAX_TASK_LIST_NAME_LEN];
} EditListData;

typedef struct
{
  unsigned char list_id;
} GetTasksData;

typedef struct
{
  unsigned char list_id;
  char title[MAX_TASK_TITLE_LEN];
  time_t deadline;
  char description[MAX_TASK_DESC_LEN];
} CreateTaskData;

typedef struct
{
  unsigned char list_id;
  unsigned char task_id;
} DeleteTaskData;

typedef struct
{
  unsigned char list_id;
  unsigned char task_id;
  char title[MAX_TASK_TITLE_LEN];
  time_t deadline;
  char description[MAX_TASK_DESC_LEN];
} EditTaskData;

typedef union
{
  LogInData lid;
  CreateListData cld;
  DeleteListData dld;
  EditListData eld;
  GetTasksData gtd;
  CreateTaskData ctd;
  DeleteTaskData dtd;
  EditTaskData etd;
} CommandData;

typedef struct
{
  char code;
  CommandData data;
} Command;

// Response Data---------------------------------------------_
typedef struct
{
  char description[ERROR_DESCRIPTION_LEN];
} ErrorResponseData;

typedef struct
{
  size_t len;
  TaskList task_lists[MAX_TASK_LISTS_PER_USER];
} GetListsResponseData;

typedef struct
{
  unsigned char id;
} CreateListResponseData;

typedef struct
{
  size_t len;
  Task tasks[MAX_TASKS_PER_LIST];
} GetTasksResponseData;

typedef struct
{
  unsigned char id;
} CreateTaskResponseData;

typedef union
{
  ErrorResponseData ed;
  GetListsResponseData gld;
  CreateListResponseData cld;
  GetTasksResponseData gtd;
  CreateTaskResponseData ctd;
} ResponseData;

typedef struct
{
  char code;
  ResponseData data;
} Response;

enum ErrorCode
{
  SUCCESS = 0x00,
  ALREADY_EXISTS = 0x01,
  NOT_FOUND = 0x02,
  NOT_PERMITTED = 0x03,
  EXCEEDED_LIMIT = 0x04
};

enum CommandCode
{
  COMMAND_SIGN_IN = 0x00,

  COMMAND_GET_LISTS = 0x10,
  COMMAND_CREATE_LIST = 0x11,
  COMMAND_DELETE_LIST = 0x12,
  COMMAND_EDIT_LIST = 0x13,

  COMMAND_GET_TASKS = 0x20,
  COMMAND_CREATE_TASK = 0x21,
  COMMAND_DELETE_TASK = 0x22,
  COMMAND_EDIT_TASK = 0x23,
};
