#pragma once
#include <time.h>

void com_sign_in(char *username, unsigned int listener_port);
void com_get_lists();
void com_create_list(char *task_list_name);
void com_delete_list(unsigned char list_id);
void com_edit_list(unsigned char list_id, char *list_name);
void com_get_tasks(unsigned char list_id);
void com_create_task(unsigned char list_id, char *task_title, char *task_desc, time_t deadline);
void com_delete_task(unsigned char list_id, unsigned char task_id);
void com_edit_task(unsigned char list_id, unsigned char task_id, char *task_title, char *task_desc, time_t deadline);
