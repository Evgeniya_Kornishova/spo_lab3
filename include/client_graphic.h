#pragma once

#include <ncurses.h>

// Windows sizes
const int MAIN_WINDOW_WIDTH;
const int MAIN_WINDOW_HEIGHT;

const int INPUT_WINDOW_WIDTH;
const int INPUT_WINDOW_HEIGHT;

const int DETAILS_WINDOW_WIDTH;
const int DETAILS_WINDOW_HEIGHT;

const int ALERT_WINDOW_WIDTH;
const int ALERT_WINDOW_HEIGHT;

const int MENU_WINDOW_WIDTH;
const int MENU_WINDOW_HEIGHT;

// Strings
const char ABOUT[34];
const char HELP_0[6];
const char HELP_1[54];
const char HELP_2[53];
const char LISTS_TITLE[6];
const char TASKS_TITLE[6];
const char TASK_TITLE[5];

// Colors
// Colors
enum COLORS
{
  COLOR_NORMAL = 1,
  COLOR_SELECTED = 2,
  COLOR_ERROR = 3,
};

enum MENU_ELEMENT_STATE
{
  MENU_EL_FOCUS = 1,
  MENU_EL_SELECTED = 2,
  MENU_EL_NORMAL = 3,
};

void init_colors();
void drawMenuBox(WINDOW *win, int ty, int lx, enum MENU_ELEMENT_STATE state, char *content);
void draw_alert(WINDOW *alert_win, char *title, char *message);
