#include "utils.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>

int max(int x, int y)
{
  return x >= y ? x : y;
}

int min(int x, int y)
{
  return x <= y ? x : y;
}

char *ltrim(char *s)
{
  while (isspace(*s))
    s++;
  return s;
}

char *rtrim(char *s)
{
  char *back = s + strlen(s);
  while (isspace(*--back))
    ;
  *(back + 1) = '\0';
  return s;
}

char *trim(char *s)
{
  return rtrim(ltrim(s));
}

time_t string_to_time_t(char *str_time)
{
  time_t result = 0;

  int year = 0, month = 0, day = 0, hour = 0, min = 0;

  if (sscanf(str_time, "%2d.%2d.%4d %2d:%2d", &day, &month, &year, &hour, &min) == 5)
  {
    struct tm breakdown = {0};
    breakdown.tm_year = year - 1900; /* years since 1900 */
    breakdown.tm_mon = month - 1;
    breakdown.tm_mday = day;
    breakdown.tm_hour = hour;
    breakdown.tm_min = min;

    if ((result = mktime(&breakdown)) == (time_t)-1)
      return 0;

    return result;
  }
  else
    return 0;
}

int is_empty_string(char *s)
{
  return (strcmp(trim(s), "") == 0);
}
