#include "file_access.h"
#include <string.h>
#include <stdlib.h>

int find_free_spot(FILE *fd, size_t offset, size_t max_records)
{
  size_t id = 0;
  char c = DB_FULL;

  while (c != DB_EMPTY && id < max_records)
  {
    fseek(fd, id * (offset + 1), SEEK_SET);
    fread(&c, 1, 1, fd);
    id++;
  }
  id--;

  // file has not free spots
  if (id >= max_records)
    return -1;

  return id;
}

void db_init(FILE *fd, size_t offset, size_t max_records)
{
  char *data = (char *)malloc(offset + 1);
  memset(data, DB_EMPTY, offset + 1);

  fwrite(data, offset + 1, max_records, fd);
}

int db_create(FILE *fd, char *data, size_t offset, size_t max_records)
{
  // find free spot
  int id = find_free_spot(fd, offset, max_records);
  if (id < 0) // file has not free spots
    return -1;

  fseek(fd, id * (offset + 1), SEEK_SET);

  // mark place
  char mark = DB_FULL;
  fwrite(&mark, 1, 1, fd);

  // write record
  fwrite(data, offset, 1, fd);

  return id;
}

void db_delete(FILE *fd, int id, size_t offset)
{
  // find line
  fseek(fd, id * (offset + 1), SEEK_SET);

  char mark = DB_EMPTY;
  fwrite(&mark, 1, 1, fd);
}

size_t db_get(FILE *fd, char **data, int id, size_t offset)
{
  // find line
  fseek(fd, id * (offset + 1), SEEK_SET);

  // check record
  char mark;
  fread(&mark, 1, 1, fd);
  if (mark == DB_EMPTY)
    return 0;

  *data = (char *)malloc(offset);
  memset(*data, DB_EMPTY, offset);

  // read row
  return fread(*data, offset, 1, fd);
}

void db_update(FILE *fd, char *data, int id, size_t offset)
{
  // find line (all lines has marks)
  fseek(fd, id * (offset + 1), SEEK_SET);

  // mark place
  char mark = DB_FULL;
  fwrite(&mark, 1, 1, fd);

  // write record
  fwrite(data, offset, 1, fd);
}
