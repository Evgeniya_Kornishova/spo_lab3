#include "task_list.h"
#include "file_access.h"
#include "assert.h"
#include <stdlib.h> // for malloc
#include <string.h> // for memset
#include <time.h>

FILE *open_or_create_task_list_db(unsigned int user_id)
{
  // create filename
  char filename[32];
  sprintf(filename, "./db/TaskLists#%u.dat", user_id);

  // open file
  FILE *fd = fopen(filename, "rb+");

  // if file doesn't exists
  if (fd == NULL)
  {
    fd = fopen(filename, "wb+");
    db_init(fd, DB_TASK_LIST_TABLE_OFFSET, MAX_TASK_LISTS_PER_USER);
  }

  return fd;
}

FILE *get_task_list_fd(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id)
{
  FILE **task_list_fd = &task_list_fds[task_list_id];
  if (*task_list_fd == NULL)
  {
    char filename[32];
    sprintf(filename, "./db/TaskList#%u_%hhu.dat", user_id, task_list_id);

    *task_list_fd = fopen(filename, "r+");

    // task list not found
    if (*task_list_fd == NULL)
      return NULL;
  }
  return *task_list_fd;
}

// return task lists info from user and task lists amount
int get_task_lists(FILE *task_lists_fd, TaskList *task_lists[])
{
  // read task lists data from file
  size_t task_lists_len = 0;
  for (int id = 0; id < MAX_TASK_LISTS_PER_USER; id++)
  {
    // read row
    size_t read_count = db_get(task_lists_fd, (char **)&task_lists[id], id, DB_TASK_LIST_TABLE_OFFSET);

    // pass empty raws
    if (read_count == 0)
      continue;

    // read user data from row and append it to list
    task_lists[id]->id = id;
    task_lists_len++;
  }

  return task_lists_len;
}

// return tasks from task list and their amount
int get_task_list(unsigned int user_id, FILE *task_list_fds[], unsigned char task_list_id, Task *task_list[])
{
  FILE *task_list_fd = get_task_list_fd(task_list_fds, user_id, task_list_id);
  if (task_list_fd == NULL)
    return -1;

  // read task list from file
  size_t task_list_len = 0;
  for (int id = 0; id < MAX_TASKS_PER_LIST; id++)
  {
    // read row
    size_t read_count = db_get(task_list_fd, (char **)&task_list[id], id, DB_TASK_TABLE_OFFSET);

    // pass empty raws
    if (read_count == 0)
      continue;

    // read user data from row and append it to list
    task_list[id]->id = id;

    task_list_len++;
  }

  return task_list_len;
}

int create_task_list(unsigned int user_id, FILE *task_lists_fd, FILE *task_list_fds[], char *task_list_name)
{
  // append record about task list to task lists user file
  TaskList *tl = (TaskList *)malloc(sizeof(TaskList));
  memset(tl, 0, sizeof(TaskList));
  tl->id = 0;
  strncpy(tl->name, task_list_name, MAX_TASK_LIST_NAME_LEN);

  int task_list_id = db_create(task_lists_fd, (char *)tl, DB_TASK_LIST_TABLE_OFFSET, MAX_TASK_LISTS_PER_USER);
  free(tl);

  // task lists already full
  if (task_list_id < 0)
  {
    return -1;
  }

  // create new empty file for task list
  char filename[32];
  sprintf(filename, "./db/TaskList#%u_%hhu.dat", user_id, task_list_id);
  FILE **task_list_fd = &task_list_fds[task_list_id];

  *task_list_fd = fopen(filename, "wb+");

  // Something went wrong and file wasn't created
  if (*task_list_fd == NULL)
    return -1;

  db_init(*task_list_fd, DB_TASK_TABLE_OFFSET, MAX_TASKS_PER_LIST);

  return task_list_id;
}

int create_task(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id, char *title, char *description, time_t deadline)
{
  FILE *task_list_fd = get_task_list_fd(task_list_fds, user_id, task_list_id);
  if (task_list_fd == NULL)
    return -1;

  Task *task = (Task *)malloc(sizeof(Task));
  memset(task, 0, sizeof(Task));

  task->id = 0;
  strncpy(task->title, title, MAX_TASK_TITLE_LEN);
  time(&task->created_at);
  task->deadline = deadline;
  strncpy(task->description, description, MAX_TASK_DESC_LEN);

  int task_id = db_create(task_list_fd, (char *)task, DB_TASK_TABLE_OFFSET, DB_TASK_TABLE_OFFSET);
  free(task);

  // task list already full
  if (task_id < 0)
  {
    return -1;
  }

  return task_id;
}

void remove_task_list(unsigned int user_id, FILE *task_lists_fd, FILE *task_list_fds[], unsigned char task_list_id)
{
  // remove file associated to task_list
  char filename[32];
  sprintf(filename, "./db/TaskList#%u_%hhu.dat", user_id, task_list_id);
  remove(filename);

  // remove file descriptor
  task_list_fds[task_list_id] = NULL;

  // remove task list from task lists file
  db_delete(task_lists_fd, task_list_id, DB_TASK_LIST_TABLE_OFFSET);
}

int remove_task(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id, unsigned char task_id)
{
  FILE *task_list_fd = get_task_list_fd(task_list_fds, user_id, task_list_id);
  if (task_list_fd == NULL)
    return -1;

  // remove task from file
  db_delete(task_list_fd, task_id, DB_TASK_TABLE_OFFSET);

  return 0;
}

void update_task_list(FILE *task_lists_fd, unsigned char task_list_id, char *task_list_name)
{
  // update task_list in task lists file
  TaskList *tl = (TaskList *)malloc(sizeof(TaskList));
  memset(tl, 0, sizeof(TaskList));

  tl->id = task_list_id;
  strncpy(tl->name, task_list_name, MAX_TASK_LIST_NAME_LEN);

  db_update(task_lists_fd, (char *)tl, task_list_id, DB_TASK_LIST_TABLE_OFFSET);

  free(tl);
}

int update_task(FILE *task_list_fds[], unsigned int user_id, unsigned char task_list_id, unsigned char task_id, char *title, char *description, time_t *deadline)
{
  FILE *task_list_fd = get_task_list_fd(task_list_fds, user_id, task_list_id);
  if (task_list_fd == NULL)
    return -1;

  // update task_list in task lists file
  Task *task;

  db_get(task_list_fd, (char **)&task, (int)task_id, DB_TASK_TABLE_OFFSET);

  if (title != NULL)
    strncpy(task->title, title, MAX_TASK_TITLE_LEN);

  if (description != NULL)
    strncpy(task->description, description, MAX_TASK_DESC_LEN);

  if (deadline != NULL)
    task->deadline = *deadline;

  db_update(task_list_fd, (char *)task, task_id, DB_TASK_TABLE_OFFSET);
  free(task);

  return 0;
}

/*
int main()
{
  // users_file = fopen("./test_data/users_test.dat", "wb+");
  //------------------------------------------------------------------
  // initial config
  // db_init(users_file, DB_USER_TABLE_OFFSET, MAX_USERS);
  // char initial_content0[] = "keks\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
  // db_create(users_file, initial_content0, DB_USER_TABLE_OFFSET, MAX_USERS);
  // char initial_content1[] = "muffin\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
  // db_create(users_file, initial_content1, DB_USER_TABLE_OFFSET, MAX_USERS);
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // open task lists file and init (empty)
  printf("Open db for task lists...\t");
  FILE *task_lists_fd = NULL;
  unsigned int user_id = 3;

  task_lists_fd = open_or_create_task_list_db(user_id);

  assert(task_lists_fd != NULL);
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // get task lists (empty)
  printf("Get empty task lists...\t");

  TaskList *task_lists[MAX_TASK_LISTS_PER_USER] = {NULL};

  int task_lists_amount = get_task_lists(task_lists_fd, task_lists);

  assert(task_lists_amount == 0 && task_lists_fd != NULL);
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // create task list
  printf("Create some task lists...\t");

  FILE *task_list_fds[MAX_TASK_LISTS_PER_USER];

  char task_list_name0[] = "Test task list0";
  int task_list0_id = create_task_list(user_id, task_lists_fd, task_list_fds, task_list_name0);

  char task_list_name1[] = "Test task list1";
  int task_list1_id = create_task_list(user_id, task_lists_fd, task_list_fds, task_list_name1);

  assert(task_list0_id == 0 && task_list1_id == 1);

  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // get task list (after adding)
  printf("Get task lists...\t");
  int task_lists_len = get_task_lists(task_lists_fd, task_lists);

  TaskList *tl_0 = task_lists[0];
  TaskList *tl_1 = task_lists[1];

  assert(task_lists_len == 2 && tl_0 != NULL && tl_1 != NULL && tl_0->id == 0 && strcmp(tl_0->name, task_list_name0) == 0 && tl_1->id == 1 && strcmp(tl_1->name, task_list_name1) == 0);

  //
  //------------------------------------------------------------------

  for (int id = 0; id < MAX_TASK_LISTS_PER_USER; id++)
    if (task_lists[id] != NULL)
    {
      free(task_lists[id]);
      task_lists[id] = NULL;
    }

  //------------------------------------------------------------------
  // update task list
  printf("Update task list...\t");

  char task_list_new_name0[] = "New task list0";
  update_task_list(task_lists_fd, task_list0_id, task_list_new_name0);

  char task_list_new_name1[] = "New task list1";
  update_task_list(task_lists_fd, task_list1_id, task_list_new_name1);

  task_lists_len = get_task_lists(task_lists_fd, task_lists);

  tl_0 = task_lists[0];
  tl_1 = task_lists[1];

  assert(task_lists_len == 2 && tl_0 != NULL && tl_1 != NULL && tl_0->id == 0 && strcmp(tl_0->name, task_list_new_name0) == 0 && tl_1->id == 1 && strcmp(tl_1->name, task_list_new_name1) == 0);

  //
  //------------------------------------------------------------------

  for (int id = 0; id < MAX_TASK_LISTS_PER_USER; id++)
    if (task_lists[id] != NULL)
    {
      free(task_lists[id]);
      task_lists[id] = NULL;
    }

  //------------------------------------------------------------------
  // remove task list
  printf("Remove task list...\t");
  remove_task_list(user_id, task_lists_fd, task_list_fds, task_list0_id);

  task_lists_len = get_task_lists(task_lists_fd, task_lists);

  tl_0 = task_lists[0];
  tl_1 = task_lists[1];

  assert(task_lists_len == 1 && tl_0 == NULL && tl_1 != NULL && tl_1->id == 1 && strcmp(tl_1->name, task_list_new_name1) == 0);

  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // get task (empty)
  Task *tasks[MAX_TASKS_PER_LIST] = {NULL};

  int task_list_len = get_task_list(user_id, task_list_fds, task_list1_id, tasks);
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // create task
  printf("Create tasks...\t");
  char title_0[] = "Test title 0";
  char description_0[] = "Description 0";
  time_t deadline_0 = time(NULL);

  int task_id_0 = create_task(task_list_fds, user_id, task_list1_id, title_0, description_0, deadline_0);

  char title_1[] = "Test title 1";
  char description_1[] = "Description 1";
  time_t deadline_1 = time(NULL);

  int task_id_1 = create_task(task_list_fds, user_id, task_list1_id, title_1, description_1, deadline_1);

  assert(task_id_0 == 0 && task_id_1 == 1);
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // get task (after adding)
  printf("Get tasks from list...\t");

  Task *task_list[MAX_TASKS_PER_LIST] = {NULL};
  int tasks_len = get_task_list(user_id, task_list_fds, task_list1_id, task_list);

  Task *t_0 = task_list[0];
  Task *t_1 = task_list[1];

  assert(tasks_len == 2 && t_0 != NULL && t_1 != NULL && t_0->id == 0 && strcmp(t_0->title, title_0) == 0 && strcmp(t_0->description, description_0) == 0 && t_0->deadline == deadline_0 && t_1->id == 1 && strcmp(t_1->title, title_1) == 0 && strcmp(t_1->description, description_1) == 0 && t_1->deadline == deadline_1);
  //
  //------------------------------------------------------------------

  for (int id = 0; id < MAX_TASKS_PER_LIST; id++)
    if (task_list[id] != NULL)
    {
      free(task_list[id]);
      task_list[id] = NULL;
    }

  //------------------------------------------------------------------
  // update task
  printf("Update tasks...\t");

  char new_title[] = "New title";
  char new_description[] = "New description";
  time_t new_deadline = time(NULL);

  int status_0 = update_task(task_list_fds, user_id, task_list1_id, task_id_0, new_title, new_description, &new_deadline);

  int status_1 = update_task(task_list_fds, user_id, task_list1_id, task_id_1, NULL, NULL, NULL);

  tasks_len = get_task_list(user_id, task_list_fds, task_list1_id, task_list);

  t_0 = task_list[0];
  t_1 = task_list[1];

  assert(tasks_len == 2 && status_0 == 0 && status_1 == 0 && t_0 != NULL && t_1 != NULL && t_0->id == 0 && strcmp(t_0->title, new_title) == 0 && strcmp(t_0->description, new_description) == 0 && t_0->deadline == new_deadline && t_1->id == 1 && strcmp(t_1->title, title_1) == 0 && strcmp(t_1->description, description_1) == 0 && t_1->deadline == deadline_1);
  //
  //------------------------------------------------------------------

  for (int id = 0; id < MAX_TASKS_PER_LIST; id++)
    if (task_list[id] != NULL)
    {
      free(task_list[id]);
      task_list[id] = NULL;
    }

  //------------------------------------------------------------------
  // delete task
  printf("Delete task...\t");
  int remove_status = remove_task(task_list_fds, user_id, task_list1_id, task_id_0);

  tasks_len = get_task_list(user_id, task_list_fds, task_list1_id, task_list);

  t_0 = task_list[0];
  t_1 = task_list[1];

  assert(tasks_len == 1 && remove_status == 0 && t_0 == NULL && t_1 != NULL && t_1->id == 1 && strcmp(t_1->title, title_1) == 0 && strcmp(t_1->description, description_1) == 0 && t_1->deadline == deadline_1);
  //
  //------------------------------------------------------------------
}
*/
