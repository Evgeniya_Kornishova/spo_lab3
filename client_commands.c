#include "communication.h"
#include "task_list.h"
#include "user.h"
#include "client_graphic.h"
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <utils.h>
#include <ncurses.h>
#include <stdlib.h>

// main data socket
extern int sock;

// user data
extern TaskList task_lists[MAX_TASK_LISTS_PER_USER];
extern Task task_list[MAX_TASKS_PER_LIST];
extern int task_lists_len;
extern int task_list_len;
extern WINDOW *alert_window;

// abort
extern void abort_client();

void execute_command(Command *com, Response *resp)
{

  int sended_bytes = send(sock, (char *)com, sizeof(Command), 0);
  if (sended_bytes < 0)
  {
    draw_alert(alert_window, "CONNECTION_ERROR", "server doesn't respond");
    abort_client();
    exit(0);
  }

  ssize_t total_bytes_readed = 0;
  while (total_bytes_readed != sizeof(Response))
  {
    ssize_t bytes_readed = read(sock,
                                &((char *)(resp))[total_bytes_readed],
                                min(sizeof(Response) - total_bytes_readed, 512));
    if (bytes_readed < 0)
    {
      draw_alert(alert_window, "CONNECTION_ERROR", "server doesn't respond");
      abort_client();
      exit(0);
    }
    total_bytes_readed += bytes_readed;
  }
}

void com_sign_in(char *username, unsigned short listener_port)
{
  Command com = {0};
  Response resp;

  com.code = COMMAND_SIGN_IN;
  strncpy(com.data.lid.username, username, MAX_USERNAME_LEN);
  com.data.lid.listener_port = listener_port;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
    abort_client();
    exit(0);
  }
}

void com_get_lists()
{
  Command com;
  Response resp;

  com.code = COMMAND_GET_LISTS;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }

  task_lists_len = resp.data.gld.len;

  memcpy(task_lists, resp.data.gld.task_lists, sizeof(TaskList) * task_lists_len);
}

void com_create_list(char *task_list_name)
{
  Command com = {0};
  Response resp;

  com.code = COMMAND_CREATE_LIST;
  strncpy(com.data.cld.name, task_list_name, MAX_TASK_LIST_NAME_LEN);

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }

  int list_id = resp.data.cld.id;
}

void com_delete_list(unsigned char list_id)
{
  Command com;
  Response resp;

  com.code = COMMAND_DELETE_LIST;
  com.data.dld.list_id = list_id;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }
}

void com_edit_list(unsigned char list_id, char *list_name)
{
  Command com = {0};
  Response resp;

  com.code = COMMAND_EDIT_LIST;
  com.data.eld.list_id = list_id;
  strncpy(com.data.eld.name, list_name, MAX_TASK_LIST_NAME_LEN);

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }
}

void com_get_tasks(unsigned char list_id)
{
  Command com;
  Response resp;

  com.code = COMMAND_GET_TASKS;
  com.data.gtd.list_id = list_id;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }

  task_list_len = resp.data.gtd.len;

  memcpy(task_list, resp.data.gtd.tasks, sizeof(Task) * task_list_len);
}

void com_create_task(unsigned char list_id, char *task_title, char *task_desc, time_t deadline)
{
  Command com = {0};
  Response resp;

  com.code = COMMAND_CREATE_TASK;
  com.data.ctd.list_id = list_id;
  strncpy(com.data.ctd.title, task_title, MAX_TASK_TITLE_LEN);
  strncpy(com.data.ctd.description, task_desc, MAX_TASK_DESC_LEN);
  com.data.ctd.deadline = deadline;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }

  int task_id = resp.data.ctd.id;
}

void com_delete_task(unsigned char list_id, unsigned char task_id)
{
  Command com;
  Response resp;

  com.code = COMMAND_DELETE_TASK;
  com.data.dtd.list_id = list_id;
  com.data.dtd.task_id = task_id;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }
}

void com_edit_task(unsigned char list_id, unsigned char task_id, char *task_title, char *task_desc, time_t deadline)
{
  Command com = {0};
  Response resp;

  com.code = COMMAND_EDIT_TASK;
  com.data.etd.list_id = list_id;
  com.data.etd.task_id = task_id;

  if (task_title != NULL)
    strncpy(com.data.etd.title, task_title, MAX_TASK_TITLE_LEN);
  if (task_desc != NULL)
    strncpy(com.data.etd.description, task_desc, MAX_TASK_TITLE_LEN);

  com.data.etd.deadline = deadline;

  execute_command(&com, &resp);

  if (resp.code != SUCCESS)
  {
    draw_alert(alert_window, "SERVER ERROR", resp.data.ed.description);
  }
}
