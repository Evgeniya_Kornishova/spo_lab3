#include <stdio.h>

void assert(int x)
{
  if (x)
    printf("\033[0;32mDone");
  else
    printf("\033[0;31mFail");
  // restore color
  printf("\033[0m\n");
}
