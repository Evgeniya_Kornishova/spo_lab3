#include "list.h"
#include <stdio.h>
#include <stdlib.h>

int list_append(List *list, void *value)
{
  Node *node = (Node *)malloc(sizeof(Node));
  if (node == NULL)
  {
    perror("List memory allocation error");
    exit(-1);
  }

  node->next = NULL;
  node->value = value;

  if (list->length == 0)
  {
    list->head = node;
    list->tail = node;
    list->length = 1;
  }
  else
  {
    list->tail->next = node;
    list->tail = node;
    list->length++;
  }

  return 0;
}

int list_remove(List *list, unsigned int pos)
{
  if (pos >= list->length)
  {
    perror("List index out of range");
    return -1;
  }

  Node *prev = NULL;
  Node *cur = list->head;

  while (pos--)
  {
    prev = cur;
    cur = cur->next;
  }

  if (prev)
    prev->next = cur->next; // if not first
  else
    list->head = cur->next; // if first

  if (!cur->next)
    list->tail = prev; // if last

  list->length--;

  free(cur->value);
  free(cur);
  return 0;
}

int list_insert(List *list, unsigned int pos, void *value)
{
  if (pos > list->length)
  {
    perror("List index out of range");
    return -1;
  }

  Node *node = (Node *)malloc(sizeof(Node));
  if (node == NULL)
  {
    perror("List memory allocation error");
    exit(-1);
  }

  node->next = NULL;
  node->value = value;

  Node *prev = NULL;
  Node *cur = list->head;

  while (pos--)
  {
    prev = cur;
    cur = cur->next;
  }

  if (prev)
    prev->next = node; // if not first
  else
    list->head = node; // if first

  node->next = cur;

  if (!node->next)
    list->tail = node; // if last

  list->length++;
  return 0;
}

void *list_get(List *list, unsigned int pos)
{
  if (pos >= list->length)
  {
    perror("List index out of range");
    return 0;
  }

  Node *cur = list->head;

  while (pos--)
  {
    cur = cur->next;
  }

  return cur->value;
}

void clean_list(List *list)
{
  Node *prev = NULL;
  for (Node *n = list->head; n != NULL; n = n->next)
  {
    if (prev)
      free(prev);

    free(n->value);
    prev = n;
  }
  if (prev)
    free(prev);

  list->head = NULL;
  list->tail = NULL;
  list->length = 0;
}

static void assert_equal(int expected, int given)
{
  if (expected == given)
    printf("\033[0;32mAssert OK\n");
  else
    printf("\033[0;31mAssert failure: expected %d, given %d\n", expected, given);

  // restore color
  printf("\033[0m");
}

static void assert_broken_pointer(int *given)
{
  if (given == 0 || given == (int *)-1)
    printf("\033[0;32mAssert OK\n");
  else
    printf("\033[0;31mAssert failure: pointer is not 0 or -1: %p\n", given);

  // restore color
  printf("\033[0m");
}

/*
int main()
{
  List test_list = EMPTY_LIST;

  // Append test--------------------------------------------------------------
  int append_test_value[] = {1, 12, 2, 17};

  printf("Append and get test\n");
  list_append(&test_list, &append_test_value[0]);
  list_append(&test_list, &append_test_value[1]);
  list_append(&test_list, &append_test_value[2]);
  list_append(&test_list, &append_test_value[3]);

  for (int i = 0; i < sizeof(append_test_value) / sizeof(int); i++)
    assert_equal(append_test_value[i], *(int *)list_get(&test_list, i));

  printf("Done!\n");

  // Insert test--------------------------------------------------------------
  int insert_test_value[] = {28, 14, 13};

  printf("Insert test\n");
  list_insert(&test_list, 0, &insert_test_value[0]);
  assert_equal(insert_test_value[0], *(int *)list_get(&test_list, 0));

  list_insert(&test_list, 5, &insert_test_value[1]);
  assert_equal(insert_test_value[1], *(int *)list_get(&test_list, 5));

  list_insert(&test_list, 3, &insert_test_value[2]);
  assert_equal(insert_test_value[2], *(int *)list_get(&test_list, 3));
  printf("Done!\n");

  // Remove test--------------------------------------------------------------
  printf("Remove test\n");
  list_remove(&test_list, 0);
  assert_equal(append_test_value[0], *(int *)list_get(&test_list, 0));

  list_remove(&test_list, 5);
  assert_broken_pointer(list_get(&test_list, 5));

  list_remove(&test_list, 3);
  assert_equal(append_test_value[3], *(int *)list_get(&test_list, 3));
  printf("Done!\n");
}
*/
