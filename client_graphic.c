#include "client_graphic.h"
#include <string.h>

// Windows sizes
const int MAIN_WINDOW_WIDTH = 100;
const int MAIN_WINDOW_HEIGHT = 35;

const int INPUT_WINDOW_WIDTH = 100;
const int INPUT_WINDOW_HEIGHT = 11;

const int DETAILS_WINDOW_WIDTH = 45;
const int DETAILS_WINDOW_HEIGHT = 18;

const int ALERT_WINDOW_WIDTH = 40;
const int ALERT_WINDOW_HEIGHT = 5;

const int MENU_WINDOW_WIDTH = 47;
const int MENU_WINDOW_HEIGHT = 18;

// Strings
const char ABOUT[] = "Kornishova E.A. Task Planner v1.0";
const char HELP_0[] = "Help:";
const char HELP_1[] = "Enter - confirm; '+' - add; Del - remove; 'e' - edit;";
const char HELP_2[] = "arrows <up>/<down>/<left>/<right> - move; Esc - exit";
const char LISTS_TITLE[] = "Lists";
const char TASKS_TITLE[] = "Tasks";
const char TASK_TITLE[] = "Task";

// Menu boxes
const int MENU_BOX_X_LEN = 19;
const int MENU_BOX_Y_LEN = 2;

// Exports
extern void print_menu();
extern void print_task_details();

void init_colors()
{
  init_pair(COLOR_NORMAL, COLOR_WHITE, COLOR_BLACK);
  init_pair(COLOR_SELECTED, COLOR_GREEN, COLOR_BLACK);
  init_pair(COLOR_ERROR, COLOR_WHITE, COLOR_RED);
}

void drawMenuBox(WINDOW *win, int ty, int lx, enum MENU_ELEMENT_STATE state, char *content)
{
  int x0, y0, x1, y1;
  x0 = lx;
  y0 = ty;
  x1 = lx + MENU_BOX_X_LEN;
  y1 = ty + MENU_BOX_Y_LEN;

  enum COLORS text_color;
  enum COLORS border_color;
  switch (state)
  {
  case MENU_EL_FOCUS:
    text_color = COLOR_SELECTED;
    border_color = COLOR_SELECTED;
    break;
  case MENU_EL_SELECTED:
    text_color = COLOR_SELECTED;
    border_color = COLOR_NORMAL;
    break;
  case MENU_EL_NORMAL:
    text_color = COLOR_NORMAL;
    border_color = COLOR_NORMAL;
    break;
  }

  char formatted_content[17];
  sprintf(formatted_content, "%16s", content);

  wcolor_set(win, border_color, NULL);

  mvwhline(win, y0, x0, '-', x1 - x0);
  mvwhline(win, y1, x0, '-', x1 - x0);
  mvwvline(win, y0, x0, '|', y1 - y0);
  mvwvline(win, y0, x1, '|', y1 - y0);
  mvwaddch(win, y0, x0, '+');
  mvwaddch(win, y1, x0, '+');
  mvwaddch(win, y0, x1, '+');
  mvwaddch(win, y1, x1, '+');

  wcolor_set(win, text_color, NULL);

  mvwaddstr(win, ty + 1, lx + 2, formatted_content);

  wcolor_set(win, COLOR_NORMAL, NULL);
}

void draw_alert(WINDOW *alert_win, char *title, char *message)
{
  werase(alert_win);

  wcolor_set(alert_win, COLOR_ERROR, NULL);
  box(alert_win, '!', '!');

  wcolor_set(alert_win, COLOR_NORMAL, NULL);
  int y, x;
  getmaxyx(alert_win, y, x);
  mvwaddstr(alert_win, 1, (x - strlen(title)) / 2, title);
  mvwaddstr(alert_win, 2, (x - strlen(message)) / 2, message);

  wrefresh(alert_win);

  getch();
  werase(alert_win);
  wrefresh(alert_win);

  print_menu();
  print_task_details();
}
