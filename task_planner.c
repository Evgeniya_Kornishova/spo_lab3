#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "server.h"
#include "client.h"

const char SERVER_DEFAULT_HOST[] = "127.0.0.1";
const int SERVER_DEFAULT_PORT = 1370;

#define MAX_HOSTNAME_LEN 32

void print_help()
{
  printf("Usage: task_planner <mode> [options]\n");

  printf("Mode:\n");
  printf("\t--client -c\tstart application as a client\n");
  printf("\t--server -s\tstart application as a server\n");

  printf("\n");

  printf("Options:\n");
  printf("\t--host -h\thost address (only for client mode!)\n");
  printf("\t--port -p\thost port[1000-65535]\n");

  printf("\n");
  printf("If options aren't provided then host address and port are set by default:\n");
  printf("\tAddress: localhost\n");
  printf("\tPort: 1370\n");
}

int parse_options(int argc, char *argv[], char *host, unsigned short *port)
{
  if (argc % 2 || argc > 4)
    return -1;

  bool host_set = false;
  bool port_set = false;

  char *key;
  char *value;
  for (int i = 0; i < argc - 1; i += 2)
  {
    key = argv[i];
    value = argv[i + 1];

    if (strncmp(key, "--host", 7) == 0 || strncmp(key, "-h", 3) == 0)
    {
      strncpy(host, value, MAX_HOSTNAME_LEN);
      host_set = true;
    }
    else if (strncmp(key, "--port", 7) == 0 || strncmp(key, "-p", 3) == 0)
    {
      if (sscanf(value, "%hu", port) == 0)
        return -1;
      if (*port < 1000 || *port > 65535)
        return -1;
      port_set = true;
    }
    else
      return -1;
  }

  if (!host_set)
    strncpy(host, SERVER_DEFAULT_HOST, MAX_HOSTNAME_LEN);

  if (!port_set)
    *port = SERVER_DEFAULT_PORT;

  return 0;
}

int main(int argc, char *argv[])
{
  if (argc == 1)
  {
    print_help();
    return 0;
  }

  char *mode = argv[1];
  char *host = (char *)malloc(MAX_HOSTNAME_LEN);
  unsigned short port;

  if (strncmp(mode, "--client", 9) == 0 || strncmp(mode, "-c", 3) == 0)
  {
    int status = parse_options(argc - 2, &argv[2], host, &port);
    if (status)
    {
      free(host);
      print_help();
      return 0;
    }

    run_client(host, port);
  }
  else if (strncmp(mode, "--server", 9) == 0 || strncmp(mode, "-s", 3) == 0)
  {
    int status = parse_options(argc - 2, &argv[2], host, &port);
    if (status)
    {
      free(host);
      print_help();
      return 0;
    }

    run_server(host, port);
  }
  else
    print_help();

  free(host);
  return 0;
}
