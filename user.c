#include "user.h"
#include "file_access.h"
#include "assert.h"
#include <stdio.h>
#include <stdlib.h> // for malloc
#include <string.h> // for memset

FILE *users_file = 0;
User *users_list[MAX_USERS];

const char USERS_FILE_PATH[] = "./db/users.dat";

// read records from users_file_path or users_file and write them into users_list array
void user_read_users_list()
{
  // open users file
  if (users_file == NULL)
    users_file = fopen(USERS_FILE_PATH, "rb+");

  // if file doesn't exists
  if (users_file == NULL)
  {
    users_file = fopen(USERS_FILE_PATH, "wb+");
    db_init(users_file, DB_USER_TABLE_OFFSET, MAX_USERS);
  }

  // clean or alloc memory for users list
  memset(users_list, 0, sizeof(User *) * MAX_USERS);

  // read all users data
  User *user;
  for (size_t user_id = 0; user_id < MAX_USERS; user_id++)
  {
    // read row
    int count = db_get(users_file, (char **)&user, user_id, DB_USER_TABLE_OFFSET);

    // pass empty raws
    if (count == 0)
      continue;

    // read user data from row and append it to list
    user->id = user_id;
    users_list[user_id] = user;
  }
}

unsigned int user_create(char *username)
{
  // save user info to file
  User *user = (User *)malloc(sizeof(User));
  memset(user, 0, sizeof(User));

  user->id = 0;
  strncpy(user->name, username, MAX_USERNAME_LEN);

  int user_id = db_create(users_file, (char *)user, DB_USER_TABLE_OFFSET, MAX_USERS);

  // users limit
  if (user_id < 0)
  {
    free(user);
    return -1;
  }

  // append new user to list
  user->id = user_id;
  users_list[user_id] = user;

  return user_id;
}

unsigned int user_get_or_create(char *username)
{
  // try to find user record in list
  User *user;
  for (int user_id = 0; user_id < MAX_USERS; user_id++)
  {
    user = users_list[user_id];
    if (user != NULL)
      if (strcmp(username, user->name) == 0)
        return user->id;
  }

  // if we didn't find user info in database, then create new one
  return user_create(username);
}

void user_release_all_resources()
{
  // close users file
  if (users_file)
    fclose(users_file);
  users_file = NULL;

  // clean memory
  User *user;
  for (int user_id = 0; user_id < MAX_USERS; user_id++)
  {
    user = users_list[user_id];
    if (user != NULL)
      free(user);
  }
}

/*
int main()
{
  users_file = fopen("./test_data/users_test.dat", "wb+");
  //------------------------------------------------------------------
  // initial config
  db_init(users_file, DB_USER_TABLE_OFFSET, MAX_USERS);
  User initial_user_0;
  initial_user_0.id = 0;
  strncpy(initial_user_0.name, "keks", MAX_USERNAME_LEN);

  User initial_user_1;
  initial_user_1.id = 1;
  strncpy(initial_user_1.name, "muffin", MAX_USERNAME_LEN);

  db_create(users_file, (char *)&initial_user_0, DB_USER_TABLE_OFFSET, MAX_USERS);
  db_create(users_file, (char *)&initial_user_1, DB_USER_TABLE_OFFSET, MAX_USERS);
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // initial read test
  //------------------------------------------------------------------
  user_read_users_list();

  printf("read users from file...\t");
  User *user0 = users_list[0];
  User *user1 = users_list[1];

  assert((user0->id == 0 && strcmp(user0->name, "keks") == 0) &&
         (user1->id == 1 && strcmp(user1->name, "muffin") == 0));

  //------------------------------------------------------------------
  // create user test
  //------------------------------------------------------------------
  printf("create new user...\t");
  char *new_user_name = "pirozhnoe";
  unsigned int new_user_id = user_create(new_user_name);

  User *new_user = users_list[new_user_id];

  assert(new_user_id == 2 && new_user->id == new_user_id && strcmp(new_user->name, "pirozhnoe") == 0);

  //------------------------------------------------------------------
  // read new user data from file test
  //------------------------------------------------------------------

  printf("read new user data from file...\t");
  user_read_users_list();

  new_user = NULL;
  new_user = users_list[new_user_id];

  assert((new_user->id == new_user_id && strcmp(new_user->name, "pirozhnoe") == 0));

  //------------------------------------------------------------------
  // search user by name test
  //------------------------------------------------------------------

  printf("find user by name...\t");
  new_user_id = user_get_or_create(new_user_name);

  assert(new_user_id == 2);

  //------------------------------------------------------------------
  // create user if it not found test
  //------------------------------------------------------------------
  printf("create new user if it not found...\t");

  new_user_name = "honey";

  new_user_id = user_get_or_create(new_user_name);

  assert(new_user_id == 3);

  //------------------------------------------------------------------
  // users release resources test
  //------------------------------------------------------------------
  printf("users release resources...\t");

  user_release_all_resources();

  assert(users_file == NULL);

  return 0;
}
*/
